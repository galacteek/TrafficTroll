class TrafficTrollException(Exception):
    pass


class MissingDependencyError(TrafficTrollException):
    pass


class DependencyOutputError(TrafficTrollException):
    pass


class ConfigError(TrafficTrollException):
    pass
